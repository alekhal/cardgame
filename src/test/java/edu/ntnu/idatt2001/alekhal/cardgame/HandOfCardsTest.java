/**
 * A few unit tests for the functionality checking the cards in the hand.
 */

package edu.ntnu.idatt2001.alekhal.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    ArrayList<PlayingCard> cards;
    HandOfCards hand;

    @BeforeEach
    void setup(){
        PlayingCard tenOfSpades = new PlayingCard('S', 10);
        PlayingCard jackOfSpades = new PlayingCard('S', 11);
        PlayingCard queenOfSpades = new PlayingCard('S', 12);
        PlayingCard kingOfSpades = new PlayingCard('S', 13);
        PlayingCard aceOfSpades = new PlayingCard('S', 1);
        cards = new ArrayList<>();
        cards.add(tenOfSpades);
        cards.add(jackOfSpades);
        cards.add(queenOfSpades);
        cards.add(kingOfSpades);
        cards.add(aceOfSpades);
        hand = new HandOfCards();
        hand.addCards(cards);
    }

    @Test
    void getCardsOfHearts() {
        assertTrue(hand.isQueenOfSpades());
    }

    @Test
    void getSumOfCards() {
        assertEquals(47, hand.getSumOfCards());
    }

    @Test
    void isQueenOfSpades() {
        assertTrue(hand.isQueenOfSpades());
    }

    @Test
    void isFlush() {
        assertTrue(hand.isFlush());
    }
}