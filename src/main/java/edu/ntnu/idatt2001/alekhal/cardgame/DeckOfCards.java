package edu.ntnu.idatt2001.alekhal.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Class representing a deck of cards. This class creates a full deck and has the ability to refresh, deal or see how many cards are left.
 */

public class DeckOfCards {

    ArrayList<PlayingCard> cards;
    Random generator;

    public DeckOfCards() {
        generator = new Random();
        cards = new ArrayList<>();
        for (int i = 1; i <= 13; i++) {
            cards.add(new PlayingCard('S', i));
            cards.add(new PlayingCard('H', i));
            cards.add(new PlayingCard('D', i));
            cards.add(new PlayingCard('C', i));
        }
    }

    public Collection<PlayingCard> dealHand(int n) {
        if (n > 52 || n < 1) {
            throw new IllegalArgumentException();
        }
        if (n > cards.size()) {
            n = cards.size();
        }
        ArrayList<PlayingCard> temp = new ArrayList<>();
        int randomInt;
        for (int i = 0; i < n; i++) {
            randomInt = generator.nextInt(cards.size());
            temp.add(cards.get(randomInt));
            cards.remove(randomInt);
        }
        return temp;
    }

    public void resetCards() {
        cards = new ArrayList<>();
        for (int i = 1; i <= 13; i++) {
            cards.add(new PlayingCard('S', i));
            cards.add(new PlayingCard('H', i));
            cards.add(new PlayingCard('D', i));
            cards.add(new PlayingCard('C', i));
        }
    }

    public int getAmountOfCards() {
        return cards.size();
    }

}