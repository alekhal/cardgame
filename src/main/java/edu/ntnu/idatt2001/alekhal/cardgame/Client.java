package edu.ntnu.idatt2001.alekhal.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Client extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Class objects.
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards();

        //Objects for main screen
        Label currentHand = new Label();
        Button dealCardsBtn = new Button();
        Button refreshCardsBtn = new Button();
        Slider slider = new Slider(1, 20, 1);
        Label sliderValue = new Label("0");
        Label deckAmountLeft = new Label("Cards left in deck: 52");
        Label cardsOfHearts = new Label("No cards of hearts");
        Label valueOfCards = new Label("Value of all cards combined: 0");
        Label queenOfSpades = new Label("The hand does not contain the queen of spades");
        Label flush = new Label("The hand does not contain a flush");

        //Slider settings and setup
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1);
        slider.valueProperty().addListener((observableValue, number, t1) -> {
            sliderValue.setText(String.valueOf(t1.intValue()));
        });
        HBox sliderlayout = new HBox(10);
        sliderlayout.getChildren().addAll(dealCardsBtn, slider, sliderValue);

        //Main screen
        primaryStage.setTitle("Cardgame");
        primaryStage.setMinWidth(500);
        primaryStage.setMinHeight(400);
        dealCardsBtn.setText("Deal cards");
        dealCardsBtn.setOnAction((e) -> {
            hand.emptyHand();
            hand.addCards(deck.dealHand((int) slider.getValue()));
            currentHand.setText("Current hand: " + hand.info());
            deckAmountLeft.setText("Cards left in deck: " + deck.getAmountOfCards());
            cardsOfHearts.setText("Cards of hearts: " + hand.getCardsOfHearts());
            valueOfCards.setText("Value of all cards combined: " + hand.getSumOfCards());

            if (hand.isQueenOfSpades()) {
                queenOfSpades.setText("The hand contains the queen of spades");
            } else {
                queenOfSpades.setText("The hand does not contain the queen of spades");
            }

            if (hand.isFlush()) {
                flush.setText("The hand contains a flush");
            } else {
                flush.setText("The hand does not contain a flush");
            }
        });

        refreshCardsBtn.setText("Refresh cards in deck");
        refreshCardsBtn.setOnAction((e)->{
            deck.resetCards();
            hand.emptyHand();
            deckAmountLeft.setText("Cards left in deck: " + deck.getAmountOfCards());
            currentHand.setText("Current hand: " + hand.info());
            cardsOfHearts.setText("Cards of hearts: " + hand.getCardsOfHearts());
            valueOfCards.setText("Value of all cards combined: " + hand.getSumOfCards());
            queenOfSpades.setText("The hand does not contain the queen of spades");
            flush.setText("The hand does not contain a flush");
        });
        currentHand.setText("Current hand: " + hand.info());

        //
        VBox root = new VBox(10);
        root.setPadding(new Insets(10, 10, 10, 10));
        root.getChildren().addAll(currentHand, sliderlayout, refreshCardsBtn, deckAmountLeft, cardsOfHearts, valueOfCards, queenOfSpades, flush);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }

}