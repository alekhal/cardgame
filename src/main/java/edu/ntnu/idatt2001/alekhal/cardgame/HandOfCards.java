package edu.ntnu.idatt2001.alekhal.cardgame;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collector;

/**
 * Class representing a hand of cards.
 * This class also does tests to see some special cases of card combinations available.
 */

public class HandOfCards {
    ArrayList<PlayingCard> cards;

    public HandOfCards() {
        cards = new ArrayList<>();
    }

    public void addCards(Collection<PlayingCard> cards) {
        this.cards.addAll(cards);
    }

    public void emptyHand() {
        cards = new ArrayList<>();
    }

    public String info() {
        StringBuilder info = new StringBuilder();
        for (PlayingCard card : cards) {
            info.append(card.getAsString()).append(" ");
        }
        return info.toString();
    }

    public String getCardsOfHearts() {
        StringBuilder info = new StringBuilder();

        cards.stream().filter((s) -> s.getSuit() == 'H').forEach((s) -> {
            info.append(s.getAsString()).append(" ");
        });
        return info.toString();
    }

    public int getSumOfCards() {
        int sum = cards.stream().mapToInt(PlayingCard::getFace).sum();
        return sum;
    }

    public boolean isQueenOfSpades() {
        return cards.stream().anyMatch((s) -> s.getSuit() == 'S' && s.getFace() == 12);
    }

    public boolean isFlush() {
        int spades = (int) cards.stream().filter((s) -> s.getSuit() == 'S').count();
        int hearts = (int) cards.stream().filter((s) -> s.getSuit() == 'H').count();
        int diamonds = (int) cards.stream().filter((s) -> s.getSuit() == 'D').count();
        int clubs = (int) cards.stream().filter((s) -> s.getSuit() == 'C').count();
        if (spades >= 5 || hearts >= 5 || diamonds >= 5 || clubs >= 5) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "HandOfCards{" +
                "cards=" + cards +
                '}';
    }
}
